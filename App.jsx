import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, StatusBar} from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Home from './src/components/Home';
import Menu from './src/components/Menu';
import Bayar from './src/components/Bayar';
import Profile from './src/components/Profile';
import Kode from './src/components/Kode';


const App = () => {
  const [activeMenu, setActiveMenu] = useState('Home');
  const Tab = createBottomTabNavigator();

  return (
    
    <View style={{flex: 1,  backgroundColor: '#FAFAFA'}}>
      <StatusBar backgroundColor={'#FAFAFA'} barStyle="dark-content" />
      <View
        style={{
          backgroundColor: '#FFFFFF',
          flexDirection: 'row',
          paddingVertical: 10,
          borderTopWidth: 1,
          borderTopColor: '#bdbdbd',
        }}>
        <TouchableOpacity
        onPress={() => setActiveMenu('Home')}
          style={{flex: 1, 
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: activeMenu == 'Home' ? '#1d5eff' : '#FFFFFF',
            elevation: activeMenu == 'Home' ? 2 : 0,
            paddingVertical: 12,
            borderRadius: 9,}}>
          <Icon name="home" size={22} color="#5e5ce5" />
          <Text style={{color: '#5e5ce5'}}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity
        onPress={() => setActiveMenu('Menu')}
          style={{flex: 1, 
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 12,
            borderRadius: 9,
            backgroundColor: activeMenu == 'Menu' ? '#1d5eff' : '#FFFFFF',
            elevation: activeMenu == 'Menu' ? 2 : 0,}}>
          <Icon name="boxes" size={22} color="#6e6b72" />
          <Text style={{color: activeMenu == 'Menu' ? '#FFFFFF' : '#9ea3b0'}}>
            Menu 
          </Text>
        </TouchableOpacity>
        <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
          <TouchableOpacity
          onPress={() => setActiveMenu('Kode')}
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              width: 60,
              height: 60,
              borderRadius: 30,
              backgroundColor: '#5e5ce5',
              position: 'absolute',
              zIndex: 1,
              bottom: 10,
              elevation: 3,
              borderWidth: 3,
              borderColor: '#FFFFFF',
            }}>
            <Icon name="qrcode" size={22} color="#FFFFFF" />
          </TouchableOpacity>
        </View>

        <TouchableOpacity
        onPress={() => setActiveMenu('Bayar')}
          style={{flex: 1, 
            justifyContent: 'center', 
            alignItems: 'center',
            backgroundColor: activeMenu == 'Bayar' ? '#1d5eff' : '#FFFFFF',
            elevation: activeMenu == 'Bayar' ? 2 : 0,
            paddingVertical: 12,
            borderRadius: 9,}}>
          <Icon name="sack-dollar" size={22} color="#6e6b72" />
          <Text
            style={{color: activeMenu == 'Bayar' ? '#FFFFFF' : '#9ea3b0'}}>
            Bayar
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
        onPress={() => setActiveMenu('Profile')}
          style={{flex: 1, 
            justifyContent: 'center', 
            alignItems: 'center',
            backgroundColor: activeMenu == 'Profile' ? '#1d5eff' : '#FFFFFF',
            elevation: activeMenu == 'Profile' ? 2 : 0,
            paddingVertical: 12,
            borderRadius: 9,
            }}>
          <Icon name="user" size={22} color="#6e6b72" />
          <Text style={{color: activeMenu == 'Profile' ? '#FFFFFF' : '#9ea3b0'}}>
            Profile 
          </Text>
        </TouchableOpacity>
      </View>
      {activeMenu == 'Home' && <Home />}
      {activeMenu == 'Menu' && <Menu />}
      {activeMenu == 'Bayar' && <Bayar />}
      {activeMenu == 'Profile' && <Profile />}
      {activeMenu == 'Kode' && <Kode />}
    </View>
  );
};

export default App;
