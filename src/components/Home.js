import React, {useState, useEffect} from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {View, Text} from 'react-native';

const Home = () => {
  return (
    <View style={{
      marginHorizontal: 0,
      flex: 1,
      backgroundColor: '#D8BFD8',
      justifyContent: 'center',
      alignItems: 'center',}}>
      <Text style={{fontSize: 22, color: '#000000'}}>Warung Ummi</Text>
      <Text style={{color: '#000000'}}>Selamat Datang Di Warung Makan Lesehan</Text>
    </View>
  );
};

export default Home;
