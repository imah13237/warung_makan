import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import home from './component';


const Stack = createNativeStackNavigator();

const component = () => {
  return (
    <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} options={{ headerShown: false}} />
      </Stack.Navigator>
  )
}

export default component

const styles = StyleSheet.create({})